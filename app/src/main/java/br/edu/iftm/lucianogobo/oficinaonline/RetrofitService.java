package br.edu.iftm.lucianogobo.oficinaonline;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RetrofitService {

    @FormUrlEncoded
    @POST("post")
    Call<Pessoa> converterUnidade(@Field("nome") String nome,
                                  @Field("senha") String senha,
                                  @Field("endereco") String endereco);

}
