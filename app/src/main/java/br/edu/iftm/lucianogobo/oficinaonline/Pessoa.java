package br.edu.iftm.lucianogobo.oficinaonline;

public class Pessoa {

    private String nome;
    private String senha;
    private String endereco;
    private boolean valid;

    public Pessoa(String nome, String senha, String endereco) {
        this.nome = nome;
        this.senha = senha;
        this.endereco = endereco;
    }

    public Pessoa() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
