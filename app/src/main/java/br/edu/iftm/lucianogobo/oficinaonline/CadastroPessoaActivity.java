package br.edu.iftm.lucianogobo.oficinaonline;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CadastroPessoaActivity extends AppCompatActivity {

    private Button btnClientCadastrarCadastro;
    private EditText etxtNameClientCadastro;
    private EditText etxtPasswordClienteCadastro;
    private EditText etxtAddressClientCadastro;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_pessoa);
        btnClientCadastrarCadastro=findViewById(R.id.btnClientCadastrarCadastro);
        etxtNameClientCadastro=findViewById(R.id.etxtNameClientCadastro);
        etxtPasswordClienteCadastro= findViewById(R.id.etxtPasswordClienteCadastro);
        etxtAddressClientCadastro=findViewById(R.id.etxtAddressClientCadastro);

    }



            //@Override
            public void onClick(View v) {
                progress = new ProgressBar(CadastroPessoaActivity.this);
                progress.setTooltipText("enviando...");
                progress.isShown();


                //pega os valores dos edittextos
                String  nome =  etxtNameClientCadastro.getText().toString();
                String senha = etxtPasswordClienteCadastro.getText().toString();
                String endereco = etxtAddressClientCadastro.getText().toString();

                //chama o retrofit para fazer a requisição no webservice
                retrofitSend(nome, senha, endereco);

            }



    public void retrofitSend(String nome, String senha, String endereco) {

        RetrofitService service = ServiceGenerator.createService(RetrofitService.class);
        Call<Pessoa> call = service.converterUnidade(nome,senha,endereco);
        call.enqueue(new Callback<Pessoa>() {
            @Override
            public void onResponse(Call<Pessoa> call, Response<Pessoa> response) {
                if (response.isSuccessful()) {
                    Pessoa pessoa1 = response.body();
                    //verifica aqui se o corpo da resposta não é nulo
                    if (pessoa1 != null) {
                        if(pessoa1.isValid()) {
                            pessoa1.setNome(pessoa1.getNome());
                            pessoa1.setSenha(pessoa1.getSenha());
                            pessoa1.setEndereco(pessoa1.getEndereco());
                            pessoa1.setValid(pessoa1.isValid());
                            progress.setVisibility(View.GONE);
         //                   setaValores();
                        } else{
                            Toast.makeText(getApplicationContext(),"Insira um nome válido", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(),"Insira uma senha valida", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),"Resposta não foi sucesso", Toast.LENGTH_SHORT).show();
                    // segura os erros de requisição
                    ResponseBody errorBody = response.errorBody();
                }
                progress.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Call<Pessoa> call, Throwable t) {

                Toast.makeText(getApplicationContext(),"Erro na chamada ao servidor", Toast.LENGTH_SHORT).show();
            }
        });

    }
}