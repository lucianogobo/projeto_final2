package br.edu.iftm.lucianogobo.oficinaonline;

import android.os.Parcel;
import android.os.Parcelable;

import java.time.LocalDate;

public class OrdemServico implements Parcelable {

    private int codigo_ordem_servico;
    private int codigo_servico;
    private int codigo_produto;
    private boolean statusreparo;
    private String atualicacao;
    private LocalDate data_criacao;
    private LocalDate data_modificacao;

    public OrdemServico(int codigo_ordem_servico, int codigo_servico, int codigo_produto, boolean statusreparo, String atualicacao, LocalDate data_criacao, LocalDate data_modificacao) {
        this.codigo_ordem_servico = codigo_ordem_servico;
        this.codigo_servico = codigo_servico;
        this.codigo_produto = codigo_produto;
        this.statusreparo = statusreparo;
        this.atualicacao = atualicacao;
        this.data_criacao = data_criacao;
        this.data_modificacao = data_modificacao;
    }

    public OrdemServico() {

    }

    public int getCodigo_ordem_servico() {
        return codigo_ordem_servico;
    }

    public void setCodigo_ordem_servico(int codigo_ordem_servico) {
        this.codigo_ordem_servico = codigo_ordem_servico;
    }

    public int getCodigo_servico() {
        return codigo_servico;
    }

    public void setCodigo_servico(int codigo_servico) {
        this.codigo_servico = codigo_servico;
    }

    public int getCodigo_produto() {
        return codigo_produto;
    }

    public void setCodigo_produto(int codigo_produto) {
        this.codigo_produto = codigo_produto;
    }

    public boolean isStatusreparo() {
        return statusreparo;
    }

    public void setStatusreparo(boolean statusreparo) {
        this.statusreparo = statusreparo;
    }

    public String getAtualicacao() {
        return atualicacao;
    }

    public void setAtualicacao(String atualicacao) {
        this.atualicacao = atualicacao;
    }

    public LocalDate getData_criacao() {
        return data_criacao;
    }

    public void setData_criacao(LocalDate data_criacao) {
        this.data_criacao = data_criacao;
    }

    public LocalDate getData_modificacao() {
        return data_modificacao;
    }

    public void setData_modificacao(LocalDate data_modificacao) {
        this.data_modificacao = data_modificacao;
    }

    //Implementação do Parcelable

    protected OrdemServico(Parcel in) {
        codigo_ordem_servico = in.readInt();
        codigo_servico = in.readInt();
        codigo_produto = in.readInt();
        statusreparo = in.readByte() != 0;
        atualicacao = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(codigo_ordem_servico);
        dest.writeInt(codigo_servico);
        dest.writeInt(codigo_produto);
        dest.writeByte((byte) (statusreparo ? 1 : 0));
        dest.writeString(atualicacao);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OrdemServico> CREATOR = new Creator<OrdemServico>() {
        @Override
        public OrdemServico createFromParcel(Parcel in) {
            return new OrdemServico(in);
        }

        @Override
        public OrdemServico[] newArray(int size) {
            return new OrdemServico[size];
        }
    };
}
