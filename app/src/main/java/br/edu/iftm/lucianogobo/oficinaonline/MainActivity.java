package br.edu.iftm.lucianogobo.oficinaonline;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<OrdemServico> ordem_servicos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.ordem_servicos= new ArrayList<>();
    }

    public void onClickNextUpdateService(View view){
        Intent listAllUpdates = new Intent(this,Historic_Service_MecShop.class);
        OrdemServico ordemServico = new OrdemServico();
        ordemServico.setAtualicacao("já consertamos o freio");
        ordemServico.setData_modificacao(LocalDate.now());
        ordemServico.setData_criacao(LocalDate.now());
        ordemServico.setCodigo_ordem_servico(1);
        ordemServico.setCodigo_produto(1);
        ordemServico.setCodigo_servico(1);
        ordemServico.setStatusreparo(true);
        this.ordem_servicos.add(ordemServico);
        listAllUpdates.putExtra(Historic_Service_MecShop.MECSHOP_LIST_KEY,this.ordem_servicos);
        startActivity(listAllUpdates);
    }

    public void onClickNextControlHistoricService(View view){
        Intent listAllServices = new Intent(this,Control_historic_service_activity.class);
        startActivity(listAllServices);
    }

    public void onClickNextCadastroPessoa(View view){
        Intent cadastroClient = new Intent(this,CadastroPessoaActivity.class);
        startActivity(cadastroClient);
    }
}