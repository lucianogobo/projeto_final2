package br.edu.iftm.lucianogobo.oficinaonline;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class Historic_Service_MecShop extends AppCompatActivity {

    public static final String MECSHOP_LIST_KEY = "Historic_Service_MecShop.MECSHOP_LIST_KEY";
    private ArrayList<OrdemServico> ordem_servicos;
    private LinearLayout lnvlMecStoreServiceList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historic__service__mec_shop);
        this.ordem_servicos = getIntent().getParcelableArrayListExtra(MECSHOP_LIST_KEY);
        this.lnvlMecStoreServiceList=findViewById(R.id.lnlvServiceList);
        this.buildMecShopServiceList();
    }

    private void buildMecShopServiceList(){
        for(OrdemServico ordem_servico: this.ordem_servicos) {
            View view = getLayoutInflater().inflate(R.layout.new_mechop_news, this.lnvlMecStoreServiceList, false);
            TextView txtDateHourService = view.findViewById(R.id.txtDateHourService);
            TextView txtDescriptionService = view.findViewById(R.id.txtDescriptionService);
            txtDateHourService.setText("12/01/2999");
            txtDescriptionService.setText(ordem_servico.getAtualicacao());
            view.setTag(ordem_servico);
            this.lnvlMecStoreServiceList.addView(view);
        }
    }
}